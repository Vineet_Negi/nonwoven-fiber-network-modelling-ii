      module STRUCTURALDB
      
      implicit none
          
C     ------------- MODULE DESCRIPTION ---------------
          
C     Define the global shared variables and arrays
C     These shared variables and arrays form the underlying
C     framework for the rest of the modules
C     ------------------------------------------------
 
 
C     -------------------------- HISTORY -------------------------------
C     history started from 9/16/2018          
C     ------------------------------------------------------------------


      save
      
      !Primary structural data - initial_nodal_positions, nodal_positions, nodal_velocities, element defs, etc
      integer :: dofs, num_nodes, num_elements
      real, dimension(:,:), allocatable :: initial_nodal_positions, 
     *nodal_positions, nodal_velocities
      
      integer, dimension(:,:), allocatable :: elements, node_elem_map
      real, dimension(:,:), allocatable ::
     *elements_positions, elements_dir, elements_forces 
           
      ! Operational data - update freq, increment counter, flags etc.
      integer :: STDB_update_freq, STDB_update_flag, 
     *STDB_increment_counter, start_flag
      real :: cumulative_time_inc, step_time
      integer, dimension(:), allocatable :: sensorIDs
      integer, dimension(:,:), allocatable :: sensor_node_map
      
      end module STRUCTURALDB
