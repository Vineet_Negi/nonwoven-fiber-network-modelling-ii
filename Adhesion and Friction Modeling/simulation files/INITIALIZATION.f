      module INITIALIZATION

      implicit none

C     ------------- MODULE DESCRIPTION ---------------
          
C     This modules is used to  perform the initialization operations.
C     -------------------------------------------------

C     -------------------------- HISTORY -------------------------------
C     history started from 9/16/2018          
C     ------------------------------------------------------------------
                
      ! Operational data - flags
      
      contains
      
      subroutine INITIALIZE()

C         SUBROUTINE DESCRIPTION:
C         This subroutine reads the user data (like element def, etc.)
C         at the starting of the program

          use STRUCTURALDB
          use GEOMPROCESSING
          
          ! declare data 
          integer :: i, j
          real :: temp
          
          ! open the files containing user data, nodal positions and element defs
          open(100, file=
     *    '/lore/negiv2/LCFN/finite_wavy/Lp05/Lf05/R1/userdata', 
     *    status='OLD')

          open(200, file=
     *    '/lore/negiv2/LCFN/finite_wavy/Lp05/Lf05/R1/nodes', 
     *    status='OLD')

          open(300, file=
     *    '/lore/negiv2/LCFN/finite_wavy/Lp05/Lf05/R1/elements', 
     *    status='OLD')

          open(400, file=
     *    '/lore/negiv2/LCFN/finite_wavy/Lp05/Lf05/R1/sim_box_data'
     *    , status='OLD')
          
          ! read the files
          
          ! read the user data - dofs, no. of nodes, no. of elements, STDB_update_freq
          read(100,*) dofs, num_nodes, num_elements, STDB_update_freq
          close(100)
                    
          ! read the initial nodal positions - coord_x, coord_y, coord_z 
          allocate (initial_nodal_positions(num_nodes, dofs))
          allocate (nodal_positions(num_nodes, dofs))
          allocate (nodal_velocities(num_nodes, dofs))
          allocate (node_elem_map(num_nodes, 2))
          
          ! initialize the databases dependent/related on the nodes (num_nodes)
          do i=1, num_nodes
              read(200,*) (initial_nodal_positions(i, j), j=1, dofs)
          end do
          close(200)
          
          nodal_positions = initial_nodal_positions
          
          node_elem_map = 0
          
          !read the element definitions - n1, n2 (n1 < n2)
          allocate (elements(num_elements,2))
          allocate (elements_positions(num_elements,dofs))
          allocate (elements_dir(num_elements,dofs))
          allocate (elements_forces(num_elements,dofs))
          
          ! initialize various DB based on element number.
          do i=1, num_elements
              
              ! read and store the nodal connectivity information
              read(300,*) elements(i,1), elements(i,2)
              
              ! Initialize the node_elem_map
              if(node_elem_map(elements(i,1), 1) == 0) then
                  node_elem_map(elements(i,1), 1) = i
              else
                  node_elem_map(elements(i,1), 2) = i
              endif
                  
              if(node_elem_map(elements(i,2), 1) == 0) then
                  node_elem_map(elements(i,2), 1) = i
              else
                  node_elem_map(elements(i,2), 2) = i
              endif
              
              do j = 1, dofs
                  elements_positions(i, j) = 
     *            0.5*(nodal_positions(elements(i,1), j) + 
     *            nodal_positions(elements(i,2), j))
    
                  
                  elements_dir(i,j) = 
     *            nodal_positions(elements(i,2), j) - 
     *            nodal_positions(elements(i,1), j)
                  
              end do
              
              ! get the characteristic element size (all elements are of same size)
              if (i == 1) then
                  
                  temp = 0.0
                  
                  do j = 1, dofs
                      temp = temp + elements_dir(i,j)**2
                  end do
                  
                  char_elem_length = sqrt(temp)
              end if                 
                  
          end do
          close(300)
          
          ! initialize the relevant DS to zero
          nodal_velocities = 0.0
          elements_forces = 0.0
          
          ! read the simulation box data
          allocate (sim_box(dofs,2))
          allocate (num_cells(dofs))
          allocate (cell_size(dofs))
          allocate (elem_cell_map(num_elements, dofs))
          
          do i=1,dofs
              read(400,*) sim_box(i,1), sim_box(i,2), num_cells(i)
              cell_size(i)= (sim_box(i,2) - sim_box(i,1))/num_cells(i)
          end do
          close (400)
          
          allocate (cells(num_cells(1), num_cells(2), num_cells(3)))
          allocate (element_linked_list(num_elements))
          
          ! initialize the cells data 
          cells = 0
          element_linked_list = 0
          
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ! MISC PARAMETER SPECIFICATIONS
          
          !specify delta_s for elemental force blending/smoothing at the edges
          delta_s = 0.10
          
          !choose parallel processing, 0 ~ No, 1 ~ Yes (Note, Parallel processing feature is not fully functional yet so let it be 0)
          p_processing = 1
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             
          
      return
      end subroutine INITIALIZE
          
      end module INITIALIZATION
