C     -------------------------- HISTORY -------------------------------
C     history started from 9/16/2018          
C     ------------------------------------------------------------------

C     INCLUDE THE RELEVANT MODULES
      include 'STRUCTURALDB.f'
      include 'COHESIONDB.f'
      include 'GEOMPROCESSING.f'
      include 'INITIALIZATION.f'

      subroutine VUAMP(
     *     ampName, time, ampValueOld, dt, nprops, props, nSvars, 
     *     svars, lFlagsInfo, nSensor, sensorValues, sensorNames,	
     *     jSensorLookUpTable,
     *     AmpValueNew,
     *     lFlagsDefine,
     *     AmpDerivative, AmpSecDerivative, AmpIncIntegral)


C     Import modules
      use STRUCTURALDB
      use GEOMPROCESSING
            
      include 'VABA_PARAM.INC'
      include 'mpif.h'
      
C     time indices
      parameter (iStepTime        = 1,
     *           iTotalTime       = 2,
     *           nTime            = 2)
C     flags passed in for information
      parameter (iInitialization   = 1,
     *           iRegularInc       = 2,
     *           ikStep            = 3,
     *           nFlagsInfo        = 3)
C     optional flags to be defined
      parameter (iComputeDeriv     = 1,
     *           iComputeSecDeriv  = 2,
     *           iComputeInteg     = 3,
     *           iStopAnalysis     = 4,
     *           iConcludeStep     = 5,
     *           nFlagsDefine      = 5)
      dimension time(nTime), lFlagsInfo(nFlagsInfo),
     *          lFlagsDefine(nFlagsDefine),
     *          sensorValues(nSensor),
     *          props(nprops),
     *          sVars(nSvars)

      character*80 sensorNames(nSensor)
      character*80 ampName
      dimension jSensorLookUpTable(*)
      
      
C     ###########################################
C     use defined variables declaration
      
      real :: t ! t ~ time
      integer :: i, p_rank, ierr
      integer slen, node_id, node_dof
      real sensor_value, position
C     ############################################
C     read the relevant passed-in inputs 
      
      t = time(iTotalTime) ! get the step time
      step_time = t
      call VGETRANK(p_rank)
C     ############################################
      ! perform the updates of various databases if the flags are raised
      
      ! Create the sensor name to id mapping at the start
      if(start_flag == 1) then
          
          !************************************************************
          !The start flag is raised when the ref amplitude is called before
          !the start of the analysis. At this time the sensors provide 0.0 
          !value irrespective of the actual value of the sensor data
          ! map the sensor names to the sensor value IDs. Hence, all calculations
          ! based on the provide value could be flawed.
          ! However, the sensor name to nodeid mapping is carried out in this step
          !************************************************************
          
          allocate (sensorIDs(nSensor))
          allocate (sensor_node_map(nSensor, 2))
          do i=1, nSensor
              
              ! we assume that the ordering of the sensorName list does not change
              ! during the simulation
              
              !get the index of the sensorName
              sensorIDs(i) = IVGETSENSORID(sensorNames(i),
     *        jSensorLookUpTable)
              
              !read the name and get the node id and dof
              slen = LEN_TRIM(sensorNames(i))
              read(sensorNames(i)(3:(slen-2)), *) node_id
              read(sensorNames(i)(slen:),*) node_dof
              
              !create the mapping of sensor name to node id, dof
              sensor_node_map(i,1) = node_id
              sensor_node_map(i,2) = node_dof

          end do
          
          !drop all the flags
          start_flag = 0 !drop the start flag
          cumulative_time_inc = 0.0 ! reset the cumulative time increment

      elseif(STDB_update_flag == 1) then
          
          ! Update the structural database
          
          !update nodal positions and velocites
          do i=1, nSensor
          
              !get the node id and dof for the sensor
              node_id = sensor_node_map(i,1)
              node_dof = sensor_node_map(i,2)
              
              !get the value of the sensor
              sensor_value = sensorValues(sensorIDs(i)) 

              !get the final position of nodes
              position = initial_nodal_positions(node_id, node_dof) 
     *        + sensor_value
                
              if(cumulative_time_inc > 0.0)then
                  nodal_velocities(node_id, node_dof) =
     *            (position - nodal_positions(node_id, node_dof))/
     *            cumulative_time_inc
              end if
              
              nodal_positions(node_id, node_dof) = position
          
          end do              
          
          STDB_update_flag = 0 ! drop the STDB_update_flag
          cumulative_time_inc = 0.0 ! reset the cumulative time increment
          
      end if    
      
      !update the cumulative time increment
      cumulative_time_inc = cumulative_time_inc + dt
      
      !get the secondary geometrical data ~ cells
      call PROCESS_GEOM()
      
      !get the elemental forces (parallely - if possible, currently not functional)
      call EVAL_FORCES_3D(p_rank)
      
      !all_reduce the elemental_forces
      if(num_cpus > 1 .and. p_processing == 1)  then
          
      !all_reduce 'x' direction
      call MPI_ALLREDUCE(elements_forces(:, 1)
     *,elements_forces(:, 1), num_elements, MPI_REAL4,
     *MPI_SUM, MPI_COMM_WORLD, ierr)
      
      !all_reduce 'y' direction
      call MPI_ALLREDUCE(elements_forces(:, 2)
     *,elements_forces(:, 2), num_elements, MPI_REAL4,
     *MPI_SUM, MPI_COMM_WORLD, ierr)
            
      !all_reduce 'z' direction
      call MPI_ALLREDUCE(elements_forces(:, 3)
     *,elements_forces(:, 3), num_elements, MPI_REAL4,
     *MPI_SUM, MPI_COMM_WORLD, ierr)
          
      endif

C     ############################################
      
      ! this is a dummy amplitude
      AmpValueNew = 0.0
      
      return
      end

      
      subroutine vdload (
C    Read only (unmodifiable)variables -
     1 nBlock, ndim, stepTime, totalTime,
     2 amplitude, curCoords, velocity, dirCos, jltyp, sname,
C    Write only (modifiable) variable -
     1 value )
C
C     Import modules
      use STRUCTURALDB
      use GEOMPROCESSING
      
      include 'vaba_param.inc'
C
      dimension curCoords(nBlock,ndim), velocity(nBlock,ndim),
     1  dirCos(nBlock,ndim,ndim), value(nBlock)
      character*80 sname
C
      integer ::i,j, km, element_id
      real, dimension(3) :: position, tangent
      real :: Kz ! the forcefield constant
      real :: total_steptime, ntime
      real :: halftime, magnitude
      real :: viscocity
      
      ! *********************************************************************
      ! External Force Field Specification
      ! specify the compressive force field strength and total simulation time
      Kz = 0.0 ! compressive force field strength
      total_steptime  = 4.0 ! total simulation time
      
      
      halftime = total_steptime/2.0
      !calculate the norm_steptime
      if(stepTime <= halftime) then
          ntime = stepTime/halftime
          magnitude = (ntime**3)*(10.0 - 15.0*ntime + 6.0*(ntime**2))
      elseif(stepTime <= total_steptime) then
          ntime = (stepTime/halftime) - 1.0
          magnitude = 1.0 - 
     *    (ntime**3)*(10.0 - 15.0*ntime + 6.0*(ntime**2))
      else
          magnitude = 0.0
      endif
      ! *********************************************************************

      ! Specify the viscocity
      viscocity = 0.0


      ! *********************************************************************

      do km = 1, nBlock
          
          do i=1, 3
              !get the position
              position(i) = curCoords(km, i)
          
              !get tangent (3D only)
              tangent(i) = dirCos(nBlock,ndim,i)
          end do
          
          value(km) = 0.0
          
          if(STDB_update_flag == 0) then
              ! the database has been updated. Proceed to locate the element
              
              !get the element ID and the elemental forces (IP)
          if(position(1) > sim_box(1,1) .and. position(1) < sim_box(1,2)
     *     .and. position(2) > sim_box(2,1) .and. 
     *     position(2) < sim_box(2,2) .and.
     *     position(3) > sim_box(3,1) .and. 
     *     position(3) < sim_box(3,2)) then
              
    
                  call GET_ELEMENTID(position, tangent, element_id) ! get the element ID
              
                  ! non-conservative force
                  if(jltyp == 41) then
                      ! force in x-direction
                      value(km) = elements_forces(element_id, 1)   ! get the cohesive force
		              !value(km) = value(km) - 0.0 ! add the external force-field
                      !value(km) = value(km) - viscocity*velocity(km,1) !add the viscous force component
                  elseif(jltyp == 42) then
                      ! force in y-direction
                      value(km) = elements_forces(element_id, 2)   ! get the cohesive force
                      !value(km) = value(km) - 0.0 ! add the external force-field
                      !value(km) = value(km) - viscocity*velocity(km,2) !add the viscous force component
                  elseif(jltyp == 43) then
                      ! force in z-direction  
                      value(km) = elements_forces(element_id, 3)  ! get the cohesive force
                      value(km) = value(km) - Kz*magnitude*position(3)  ! add the external force-field
                      !value(km) = value(km) - viscocity*velocity(km,3) !add the viscous force component
                  endif
          else
              write(*,*) 'elements outside sim box'
              value(km) = 0.0
          endif            
          endif
      end do
                
      return
      end
      
      
      subroutine vexternaldb(lOp, i_Array, niArray, r_Array, nrArray)
      
      use STRUCTURALDB
      use INITIALIZATION
      use GEOMPROCESSING
C
      include 'vaba_param.inc'
      include 'mpif.h'
c
C     Contents of i_Array
      parameter( i_int_nTotalNodes     = 1,
     *           i_int_nTotalElements  = 2,
     *           i_int_kStep           = 3,
     *           i_int_kInc            = 4,
     *           i_int_iStatus         = 5,
     *           i_int_lWriteRestart   = 6  )

C     Possible values for the lOp argument
      parameter( j_int_StartAnalysis    = 0,      
     *           j_int_StartStep        = 1,      
     *           j_int_SetupIncrement   = 2,      
     *           j_int_StartIncrement   = 3,      
     *           j_int_EndIncrement     = 4,      
     *           j_int_EndStep          = 5,      
     *           j_int_EndAnalysis      = 6 )     


C     Possible values for i_Array(i_int_iStatus)
      parameter( j_int_Continue          = 0,      
     *          j_int_TerminateStep     = 1,      
     *          j_int_TerminateAnalysis = 2)      

C     Contents of r_Array
      parameter( i_flt_TotalTime   = 1,
     *           i_flt_StepTime    = 2,
     *           i_flt_dTime       = 3 )
C
      dimension i_Array(niArray),      
     *   r_Array(nrArray)
      
      ! user variables
      integer :: i, j, p_rank, lb, ub
      integer :: ierr, myid
      integer, dimension(2):: temp_array

      kStep = i_Array(i_int_kStep)
      kInc  = i_Array(i_int_kInc)


C     Start of the analysis
      if (lOp .eq. j_int_StartAnalysis) then
          
          call INITIALIZE()
          
          ! initialize flags
          start_flag = 1
          STDB_update_flag = 1
          STDB_increment_counter = 0
          
          ! initialize the parallel processing info
          ! divide the total number of elements amongst various processes
          call VGETNUMCPUS(num_cpus)
          allocate (process_elements(num_cpus,2))  
          process_elements = 0
          j = int(num_elements/num_cpus)
          i = num_elements - j*num_cpus
          call vgetrank(p_rank)
          
          ! get the lower bound of elements
          lb = j*p_rank + min(i, p_rank) + 1
          ub = j*(p_rank + 1) + min(i, p_rank + 1)
          
          if(p_processing == 1) then
              process_elements(p_rank + 1, 1) = lb
              process_elements(p_rank + 1, 2) = ub
          else
              process_elements(p_rank + 1, 1) = 1
              process_elements(p_rank + 1, 2) = num_elements
          endif         
          

          !if parallel processing used
          if(num_cpus > 1) then
              
              !all_reduce the procee_elements 
              
              call MPI_ALLREDUCE(process_elements(:, 1)
     *         , process_elements(:, 1), num_cpus, MPI_INTEGER,
     *         MPI_SUM, MPI_COMM_WORLD, ierr);
              
              call MPI_ALLREDUCE(process_elements(:, 2)
     *         , process_elements(:, 2), num_cpus, MPI_INTEGER,
     *         MPI_SUM, MPI_COMM_WORLD, ierr);
              
          endif  
          
C     Start of the step
      else if (lOp .eq. j_int_StartStep) then
         continue 

C     Setup the increment       
      else if (lOp .eq. j_int_SetupIncrement) then       
          continue
      
C     Start of the increment
      else if (lOp .eq. j_int_StartIncrement) then
          
          STDB_increment_counter = STDB_increment_counter + 1 ! increment the update counters
          
          ! re-initialize various databases
          elements_forces = 0.0
          
          ! reinitialize only the modified cells
          do i=1, num_elements
              cells(elem_cell_map(i,1), elem_cell_map(i,2),
     *        elem_cell_map(i,3)) = 0
          end do
                    
          ! raise the flags for updates
          
          !STRUCTURAL DATABASE update
          if(STDB_increment_counter >= STDB_update_freq) then
              STDB_increment_counter = 0 ! reset the increment counter
              STDB_update_flag = 1 ! raise the update flag
          end if
          

C     End of the increment
      else if (lOp .eq. j_int_EndIncrement) then
          continue

C     End of the step
      else if (lOp .eq. j_int_EndStep) then
          continue

C     End of the analysis
      else if (lOp .eq. j_int_EndAnalysis) then
          continue

      end if 

      return
      end
