      module GEOMPROCESSING
      
      implicit none
          
C     ------------- MODULE DESCRIPTION ---------------
          
C     This module contains various data structures and procedures
C     for performing the geometrical processing on the primary 
C     structural data in STRUCTURALDB
C     ------------------------------------------------

C     -------------------------- HISTORY -------------------------------
C     history started from 9/16/2018          
C     09/16/2018 - two new functions for calculating the element length
C     in initial and final config were added. eval_elemental_force(E1, 
C     E2) was modified to use these new functions
C     09/17/2018 - the sign(1.0, d0) was replaced by (d0/abs(d0)) as sign 
C     is incompatible with double precision
C     ------------------------------------------------------------------
          
      save

      ! Simulation Box dimensions
      real, dimension(:,:), allocatable :: sim_box
      
      ! No. of cells in each direction
      integer, dimension(:),  allocatable :: num_cells
      
      !dimensions of cells in each direction
      real, dimension (:), allocatable :: cell_size
      
      !elem_cell_map
      integer, dimension(:,:), allocatable:: elem_cell_map
      
      !cells
      integer, dimension(:,:,:), allocatable:: cells
      
      !element_linked_list
      integer, dimension(:), allocatable:: element_linked_list
      
      ! characterisitc element length
      real :: char_elem_length, delta_s
      
      ! Data for parallel processing of GEOMPROCESSING module
      integer:: p_processing
      integer:: num_cpus
      integer, dimension(:,:), allocatable:: process_elements
      
      contains
      
      ! internal subroutines
      
      subroutine dot(v1, v2, dot_prod)
      
C         SUBROUTINE DESCRIPTION:
C         This subroutine calculates the dot product of two vectors - v1, v2 (3D)
          
          real, dimension(3) :: v1, v2
          real :: dot_prod
          integer :: i
          
          dot_prod = 0.0
          do i=1, 3
              dot_prod = dot_prod + v1(i)*v2(i)
          end do

      return
      
      end subroutine dot  
      
      subroutine norm(v, norm_value)
      
C         SUBROUTINE DESCRIPTION:
C         This subroutine calculates the norm of a vector
          
          real, dimension(3) :: v
          real :: norm_value
          
          call dot(v, v, norm_value)
          norm_value = sqrt(norm_value)
       
      return
      
      end subroutine norm  
      
      subroutine cross(v1, v2, cross_product)
      
C         SUBROUTINE DESCRIPTION:
C         This subroutine calculates the cross-product of two vectors (3d) ~ v1, v2
      
          real, dimension(3) :: v1, v2, cross_product          

          cross_product(1) = (v1(2)*v2(3) - v1(3)*v2(2))
          cross_product(2) = (v1(3)*v2(1) - v1(1)*v2(3))    
          cross_product(3) = (v1(1)*v2(2) - v1(2)*v2(1))  
      
      return
      
      end subroutine cross 
      
      subroutine get_cur_elem_length(E, elen)
      
C         SUBROUTINE DESCRIPTION:
C         This subroutine calculates the current length of an element 'E'

          use STRUCTURALDB

          integer :: E, N1, N2
          real :: elen
          real, dimension(3) :: v
          
          ! get nodes of element 
          N1 = elements(E,1)
          N2 = elements(E,2)
                    
          ! get the elem vector
          v = nodal_positions(N2, :) - nodal_positions(N1, :)
                  
          ! get the current element length
                  
          elen = sqrt(dot_product(v, v))                 

      return
      
      end subroutine get_cur_elem_length      
      
      
      subroutine get_ini_elem_length(E, elen)
      
C         SUBROUTINE DESCRIPTION:
C         This subroutine calculates the initial length of an element 'E'

          use STRUCTURALDB

          integer :: E, N1, N2
          real :: elen
          real, dimension(3) :: v
          
          !get nodes of element 
          N1 = elements(E,1)
          N2 = elements(E,2)
          
          ! get the elem vector
          v = initial_nodal_positions(N2, :) - 
     *     initial_nodal_positions(N1, :)
                  
          ! get the current element length
                  
          elen = sqrt(dot_product(v, v))                 

    
      return
      
      end subroutine get_ini_elem_length        
      
      subroutine PROCESS_GEOM()
 

      
C         SUBROUTINE DESCRIPTION:
C         This subroutine processes the primary nodal information
C         at the starting of every increment and calculates the elemental data
C         It also updates the cells' data
      
          use STRUCTURALDB
          
          !declare the internal variables
          integer :: i, j, k
          integer :: N1, N2 
          
          !calculates the elements_position and direction
          do  i = 1, num_elements
              
              ! get the nodes of the elements
              N1 = elements(i,1)
              N2 = elements(i,2)
              
              do j = 1, dofs
                  
                  !update the positions
                  elements_positions(i, j) = 
     *            0.5*(nodal_positions(N1, j) + 
     *            nodal_positions(N2, j))
                  
                  !update the directions
                  elements_dir(i,j) = 
     *            nodal_positions(N2, j) - 
     *            nodal_positions(N1, j)
                  
                  !get the cell position for the element
                  elem_cell_map(i,j) = 
     *            int((elements_positions(i, j) - sim_box(j, 1))
     *            /cell_size(j)) + 1                      
              end do
              
              
      
              !populate the cells (linked lists)
                           
              element_linked_list(i) = cells(elem_cell_map(i,1),
     *        elem_cell_map(i,2), elem_cell_map(i,3))
             
              cells(elem_cell_map(i,1), elem_cell_map(i,2),
     *        elem_cell_map(i,3)) = i
                        
              end do
              
      return
      
      end subroutine PROCESS_GEOM     
          
      
      subroutine eval_elemental_force(E1, E2)
      
C         SUBROUTINE DESCRIPTION:
C         This subroutine calculates the force contributions for the elements E1, E2
      
          use STRUCTURALDB
          use COHESIONDB
          
          
          integer:: i
          integer:: E1, E2, N11, N12, N21, N22, temp_int
          real, dimension(3) :: E1_position, E2_position, P1, P2, b,
     *    T1, T2, u, temp_array, n_force
          real :: dc, d0, der_d0, temp_real, s1, s2, e_len1, e_len2
          real, dimension (3,2) :: A
          real :: central_force, weight, w1, w2
          real, dimension(3) :: v1,v2
          
                              
          !get nodes of element 1
          N11 = elements(E1,1)
          N12 = elements(E1,2)
          if(N11 > N12) then
              temp_int = N12
              N12 = N11
              N11 = temp_int
          end if
      
          ! get the position of element center for E1
          E1_position = elements_positions(E1,:)
          
          
          !get nodes  of element 2
          N21 = elements(E2,1)
          N22 = elements(E2,2)
          if(N21 > N22) then
              temp_int = N22
              N22 = N21
              N21 = temp_int
          end if
            
          ! get the position of element center for E2
          E2_position = elements_positions(E2,:)
          
          
          !get the distance between element centres
          temp_array = E2_position - E1_position
          dc = sqrt(dot_product(temp_array,temp_array))
          
          !check for the ends (temp_int /= 0)
          temp_int=node_elem_map(N11,2)*
     *    node_elem_map(N12,2)*node_elem_map(N21,2)*
     *    node_elem_map(N22,2)
          
          
          
          if(N11 /= N21 .and. N11 /= N22 .and. N12 /= N21 .and. 
     *    N12 /= N22 .and. temp_int /= 0) then
              
              if(dc < char_elem_length) then
              
                  !do further processing
              
                  !get the shortest distance between the element's lines
                  
                  ! get the ref points
                  P1 = nodal_positions(N11, :)
                  P2 = nodal_positions(N21, :)
                  
                  ! get the ref directions
                  T1 = nodal_positions(N12, :) - nodal_positions(N11, :)
                  T2 = nodal_positions(N22, :) - nodal_positions(N21, :)
                  
                  ! get the current element lengths
                  
                  e_len1 = sqrt(dot_product(T1, T1))
                  T1 = T1/e_len1
                  
                  e_len2 = sqrt(dot_product(T2, T2))
                  T2 = T2/e_len2
                  
                  ! find the vector perpendicular to both element dir
                  call cross(T1, T2, u)
                  
                  if(sqrt(dot_product(u,u)) > 0.0) then
                      u = u/sqrt(dot_product(u,u))
                  end if
                  
                  ! get the shortest distance (directions)
                  d0 = dot_product((P2 - P1), u)
                  
                  ! find s1 and s2 for d0
                  
                  do i =1, 3
                      A(i,1) = T2(i)
                      A(i,2) = -T1(i)
                      
                      b(i) = d0*u(i) + P1(i) - P2(i)
                  end do
                  
                  ! solve for s1 and s2
                  temp_real = (1.0/(A(1,1)*A(2,2) - A(1,2)*A(2,1)))
                  s2 =  temp_real*(A(2,2)*b(1) - A(1,2)*b(2))
                  s1 =  temp_real*(-A(2,1)*b(1) + A(1,1)*b(2))
                  
                  !normalize s1, s2
                  s1 = s1/e_len1
                  s2 = s2/e_len2
                  
                  !set default weights for the force blending at the elemental edges
                  w1 = 1.0
                  w2 = 1.0
                  
                  if(s1 >= (0.0 - delta_s) .and. s1 <= (1.0 + delta_s) 
     *             .and. s2 >= (0.0 - delta_s) .and. 
     *             s2 <= (1.0 + delta_s)) then
           
                  !set the weight or force blending at the elemental edges
                  if(s1 >= (0.0 - delta_s) .and. s1 <= (0.0 + delta_s))
     *            then
                  !w1 = 0.5 + 0.5*(s1/delta_s)
                  w1 = 0.5
                  endif
                  if(s1 >= (1.0 - delta_s) .and. s1 <= (1.0 + delta_s))
     *             then
                  !w1 = 0.5 - 0.5*((s1-1.0)/delta_s)
                  w1 = 0.5
                  endif
                  if(s2 >= (0.0 - delta_s) .and. s2 <= (0.0 + delta_s))
     *             then
                  !w2 = 0.5 + 0.5*(s2/delta_s)
                  w2 = 0.5
                  endif
                  if(s2 >= (1.0 - delta_s) .and. s2 <= (1.0 + delta_s))
     *             then
                  !w2 = 0.5 - 0.5*((s2-1.0)/delta_s)
                  w2 = 0.5
                  endif
                      
                      ! Calculate the weight for this elemental-interaction
                      weight = w1*w2
                                                      
                      ! get the relative velocity
                      do i=1, 3
                          v1(i) = (1.0 - s1)*nodal_velocities(N11, i) +
     *                    (s1)*nodal_velocities(N12, i)
                          
                          v2(i) = (1.0 - s2)*nodal_velocities(N21, i) +
     *                    (s2)*nodal_velocities(N22, i)
                      end do
                      
                      der_d0 = dot_product((v2 - v1), u)*(d0/abs(d0))
                          
                      ! central force calcaluation 
                          
                      !calculate the central force due to cohesion
                      call force_disp(central_force, abs(d0), der_d0, 
     *                step_time)   
                                                      
                      !apply weight to the central forces to blend/smooth out the forces at the elemental edges
                      central_force = weight*central_force
                                                                  
                      ! project the central forces on the nodes
                                            
                      !E1 - N1
                      n_force = central_force*(1.0 - s1)*(d0/abs(d0))*u

                      do i =1, 2
                          temp_int = node_elem_map(N11,i)
                          call get_cur_elem_length(temp_int, temp_real)
                          if(temp_int > 0) then
                              elements_forces(temp_int, :) =
     *                        elements_forces(temp_int, :) +
     *                        0.5*n_force/temp_real
                          end if
                      end do
                      
                      !E1 - N2
                      n_force = central_force*(s1)*(d0/abs(d0))*u
                     
                      do i =1, 2
                          temp_int = node_elem_map(N12,i)
                          call get_cur_elem_length(temp_int, temp_real)
                          if(temp_int > 0) then
                              elements_forces(temp_int, :) =
     *                         elements_forces(temp_int, :) +           
     *                        0.5*n_force/temp_real
                          end if                          
                      end do
                      
                      !E2 - N1
                      n_force = 
     *                -1.0*central_force*(1.0 - s2)*(d0/abs(d0))*u

                      do i =1, 2
                          temp_int = node_elem_map(N21,i)
                          call get_cur_elem_length(temp_int, temp_real)
                          if(temp_int > 0) then
                              elements_forces(temp_int, :) =
     *                         elements_forces(temp_int, :) +          
     *                        0.5*n_force/temp_real
                          end if
                      end do
                      
                      !E2 - N2
                      n_force = -1.0*central_force*(s2)*(d0/abs(d0))*u
                                            
                      do i =1, 2
                          temp_int = node_elem_map(N22,i)
                          call get_cur_elem_length(temp_int, temp_real)
                          if(temp_int > 0) then
                              elements_forces(temp_int, :) =
     *                         elements_forces(temp_int, :) +           
     *                        0.5*n_force/temp_real
                          end if
                      end do                      
                  end if
              end if
          end if
     
      return
      end subroutine eval_elemental_force
      

      subroutine EVAL_FORCES_3D(p_rank)

C         SUBROUTINE DESCRIPTION:
C         This subroutine processes process the secondary data - cells, elements
C         to determine the elements_forces

          use STRUCTURALDB
          
          !declare the internal variables (for 3D only)
          integer :: e, i, j, k, max_iter, iter, hash_C0, hash_C1
          integer :: E0, E1
          integer, dimension(3) :: C0, C1
          
          !declare variables for parallel processing
          integer :: p_rank
          
          ! initialize the internal variables
          max_iter = num_elements - 1
          
          ! loop through all the elements and process the neighboring cells
          do e = process_elements(p_rank + 1,1), 
     *        process_elements(p_rank + 1,2)
              
              ! get the base element
              E0 = e
              
              ! get the containing cell
              C0 = elem_cell_map(e,:)
              
              
              ! ******************************************************
              ! seach for elemental contacts
              
              ! search for contact in the same cell                
              ! go through all the elements
              E1 = cells(C0(1), C0(2), C0(3))
              iter = 0
              do while (E1 > 0) 
                                      
                  ! evaluate only unique pairs
                  if(E0 < E1) then
                      
                    ! evaluate the elemental forces
                      call eval_elemental_force(E0, E1)
                      
                  end if
                                                                
                  ! go to the next element
                  E1 = element_linked_list(E1)
                                      
                  ! check for error
                  iter =  iter + 1
                  if(iter >= max_iter) then
                      write(*,*) 'Error: iterations exceeded 1'
                      call exit(1)
                  end if
                                  
              end do
              
              !calculate the hash values of the base cell
              hash_C0 = C0(1) + num_cells(1)*C0(2) + 
     *        num_cells(1)*num_cells(2)*C0(3) 
              
              ! search for contact in the neighboring cells
              do i = -1, 1
                  do j = -1, 1
                      do k = -1, 1
                          
                          ! get the position of the neighboring cells
                          C1(1) = C0(1) + i
                          C1(2) = C0(2) + j
                          C1(3) = C0(3) + k
                          

                          !calculate the hash values of the neighbor cell
                          hash_C1 = C1(1) + num_cells(1)*C1(2) + 
     *                    num_cells(1)*num_cells(2)*C1(3) 
                          
                          !if the neighboring cellls are within boundaries (fixed boundaries)...this may be done periodically
                          if(C1(1) >= 0 .and. C1(1) <= num_cells(1)
     *                    .and. C1(2) >= 0 .and. C1(2) <= num_cells(2)
     *                    .and. C1(3) >= 0 .and. C1(3) <= num_cells(3))
     *                    then
                          
                          
                              ! if the cells are not  same
                              if(hash_C0 < hash_C1)
     *                        then 
                                  
                                  ! go through all the elements
                                  E1 = cells(C1(1), C1(2), C1(3))
                                  iter = 0
                                  do while (E1 > 0) 
                                      
                                      ! evaluate the elemental forces
                                      call eval_elemental_force(E0, E1)
                                      
                                      ! go to the next element
                                      E1 = element_linked_list(E1)
                                      
                                      ! check for error
                                      iter =  iter + 1
                                      if(iter >= max_iter) then
                                          write(*,*) 
     *                                    'Error: iterations exceeded 2'
                                          call exit(1)
                                      end if
                                  enddo    
                              endif                                 
                          end if                      
                      end do
                  end do
              end do
              ! ******************************************************
          end do
                           
      return
      end subroutine EVAL_FORCES_3D              
              
      
      subroutine GET_ELEMENTID(position, tangent, element_id)
      
C         SUBROUTINE DESCRIPTION:
C         This subroutine finds the element's ID (2 node element)
C         based on the Intergration-Point position and the element's orientation (tangent) in 3D
      
          use STRUCTURALDB
          
          !declare the internal variables (for 3D only)
          integer :: i, j, k, l, iter, max_iter
          integer :: element_id, E1
          integer, dimension(3) :: cell_id0, cell_id
          real, dimension(3) :: position, tangent
          integer, dimension(3,2) :: N
          
          !internal parameters for element detection
          real(kind = 8) :: eps_location ! the fraction of the element length for location detection
          real(kind = 8), dimension(3) :: boundary_gap ! to store the gap of the element position from cell boundary 
          real(kind = 8):: temp_r, 
     *      spatial_proximity, min_spatial_proximity, boundary_tol
          
          
          !initialize the internal parameters
          eps_location = 0.00001
          boundary_tol = 0.001
          max_iter = num_elements - 1
          min_spatial_proximity = char_elem_length
          N = 0

          ! get the containing cell's position
          do i=1, dofs
              
              temp_r = (position(i) - sim_box(i, 1))/cell_size(i)
              cell_id0(i) = int(temp_r) + 1
                  
              boundary_gap(i) = nint(temp_r) - temp_r 
                   
              ! check if the element is at the boundary of the cell 
              ! if it is then also process the adjacent cell
              if(abs(boundary_gap(i)) < boundary_tol) then
                  if(boundary_gap(i) > 0.0) then
                      N(i,2) = 1
                  else
                      N(i,1) = -1
                  endif
              endif
          end do
             
          ! search for the element in the neighboring cells
          do i = N(1,1), N(1,2)
              do j = N(2,1), N(2,2)
                  do k = N(3,1), N(3,2)
                      
          ! get the cell id
          cell_id(1) = cell_id0(1) + i
          cell_id(2) = cell_id0(2) + j
          cell_id(3) = cell_id0(3) + k
          
          ! go through all the elements
          E1 = cells(cell_id(1), cell_id(2), cell_id(3))
          iter = 0
                  
          do while (E1 > 0) 
              
              ! check the proximity
              spatial_proximity = 0.0
              do l=1, dofs
                  spatial_proximity = spatial_proximity + 
     *            (position(l) - elements_positions(E1,l))**2
              end do
              spatial_proximity = sqrt(spatial_proximity)
              
              ! prxomimity based criterions for element detection (absolute and relative)
              if(spatial_proximity <= eps_location*char_elem_length)then
                  min_spatial_proximity = spatial_proximity
                  element_id = E1
                  exit
              elseif (spatial_proximity < min_spatial_proximity) then
                  min_spatial_proximity = spatial_proximity
                  element_id = E1
              endif
                                   
              ! go to the next element
              E1 = element_linked_list(E1)
                                      
              !check for error
              iter =  iter + 1
              
              if(iter >= max_iter) then
                  write(*,*) 
     *            'Error: iterations exceeded 3'
                  call exit(1)
              end if
          end do
          
                  enddo
              enddo
          enddo
           
      return
      end subroutine GET_ELEMENTID

      end module GEOMPROCESSING
