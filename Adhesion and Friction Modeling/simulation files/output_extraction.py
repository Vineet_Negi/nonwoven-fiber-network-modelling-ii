# -*- coding: mbcs -*-
# Do not delete the following import lines
import numpy as np
import os
from odbAccess import *
from odbMaterial import *
from odbSection import *
import pickle

def save_obj(obj, name ):
    with open( name + '.pkl', 'wb') as f:
        pickle.dump(obj, f)

def array2Dreader(filename, delimiter):
    '''Read a file as a 2D array'''
    data = []
    templist = []
    with open(filename, 'r+') as file:
        for l in file:          
            for item in l.strip().split(delimiter):
                templist.append(float(item))
            if(len(templist) != 0):
                data.append(templist)
                
            
            templist = []
        del templist
    return data
        

# This code reads the nodal dofs of the ODB file and writes them in outDB.npy array

def GF():
  

    "specify the jobname"
    job_type = 'loading_ext'
    case = 'Fn_Adh-8_Ft_Adh-4'
    jobname = job_type + '_' + case
    
    # open the odb file and get the last frame
    odbname = 	jobname  + '.odb'
    odb = openOdb(odbname, readOnly=True)
     
    "get the node set of interest"
    #NOTE: When the results are loaded an assembly is automatically created with only 1 part - 'PART-1-1'
    INSTANCE = odb.rootAssembly.instances['PART-1-1']
    nset = INSTANCE.nodeSets['X1']

    RF = []
    for Frame in odb.steps['Loading_Step'].frames:
        "Extract the displacement and the reaction forces for the X1 set"

        displacements = Frame.fieldOutputs['U']
        reaction_forces = Frame.fieldOutputs['RF']
        disp_fieldValues = displacements.getSubset(region=nset)
        rf_fieldValues = reaction_forces.getSubset(region=nset)

        total_RF = 0.0
        num_nset = len(disp_fieldValues.values)
        for i in range(num_nset):
        
            v_RF = rf_fieldValues.values[i]
            total_RF += v_RF.data[0]

        RF.append(total_RF)
        
        

    np.save('RF_ext_R1_' + case, RF)
  
    #odb.close()
       

GF()
        
            
            
            
            

            
         
        
        

        

    
    

    
    


    
                               
                

        


        
            
        
        
