      module COHESIONDB
      
      implicit none
          
C     ------------- MODULE DESCRIPTION ---------------
C     This module stores the model/s for adhesive or cohesive forces
C     ------------------------------------------------
   
C     -------------------------- HISTORY -------------------------------
C     history started from 9/16/2018          
C     ------------------------------------------------------------------

      contains
      
      subroutine force_disp(f, d, der_d, t)
      
C         SUBROUTINE DESCRIPTION:
C         This subroutine calculates the cohesive force based on the displacement.
C         Enter the force displacement curve here
      
          ! decalre input parameters. 
          !f ~ adhesion force
          !d ~ min. gap between elements i.e. distance between closest points between two neighboring elements
          !der_d ~ rel. velocity of the closest points between two neighboring elements
          !t ~ some measure of time.
          real :: f, d, der_d, t
          
          ! declare f-d relationship property specification parameters
          real :: r, r0, r1, der_d0
          real :: K_d, f0
          real :: r_min_cutoff
          
          ! specifiy f-d relationship property specification parameters
          r0 = 0.002
          r1 = 1.05*r0
          r_min_cutoff = 0.8*r0
          K_d = 0.01
          f0 = (100.0)*(1e-8)
          der_d0 = -1e-5
          
          ! initialize the cohesion force
          f = 0.0 
          
          !specify the force-disp relationship
          ! #########################################
          if(d < r_min_cutoff) then
              ! This is to prevent the deep inter-penetration of two fibers.
	      ! This can either be set to 0.0 or a mildly repulsive force for avoiding reducing overlap offsets.
              f = -0.5*f0 
          elseif(d <= r0 ) then
              f = f0 
          elseif(d <= r1) then
              r = (d - r1)/(r0 - r1)
              f = f0
              if(der_d < der_d0) then
                  f = 0.0
              endif
          else
              f = 0.0
          endif      
          ! #########################################
          
      return
      end subroutine force_disp      
  
      end module COHESIONDB
